#!/bin/bash

brew install minikube qemu
# Install network driver
brew install socket_vmnet
brew tap homebrew/services
# activate the service
HOMEBREW=$(which brew) && sudo ${HOMEBREW} services start socket_vmnet
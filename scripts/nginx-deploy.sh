#!/bin/bash
# Deploy Nginx and expose it via a LoadBalancer service
kubens default
kubectl create deployment nginx --image=nginx
kubectl expose deployment nginx --port=80 --type=LoadBalancer


# Minikube and MetalLB Setup

This repository contains scripts and configuration files to set up a local Kubernetes cluster using Minikube with MetalLB as a LoadBalancer.

## Getting Started

### Prerequisites

- Install Minikube
- Install kubectl

### Deployment

Run the following scripts in order to set up the environment:

1. `./scripts/minikube-start.sh`
2. `./scripts/metallb-install.sh`
3. `./scripts/metallb-apply-config.sh`
4. `./scripts/nginx-deploy.sh`

Refer to each script and configuration file for more details on their operation and purpose.
